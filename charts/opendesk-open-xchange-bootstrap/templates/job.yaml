{{/*
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
*/}}
---
apiVersion: {{ include "common.capabilities.cronjob.apiVersion" . | quote }}
kind: "Job"
metadata:
  name: {{ include "common.names.fullname" . | quote }}
  labels:
    {{- include "common.labels.standard" . | nindent 4 }}
    {{- if .Values.additionalLabels }}
    {{- include "common.tplvalues.render" ( dict "value" .Values.additionalLabels "context" . ) | nindent 4 }}
    {{- end }}
  {{- if .Values.additionalAnnotations }}
  annotations: {{- include "common.tplvalues.render" ( dict "value" .Values.additionalAnnotations "context" . ) | nindent 4 }}
  {{- end }}
spec:
  {{- if .Values.cleanup.deletePodsOnSuccess }}
  ttlSecondsAfterFinished: {{ .Values.cleanup.deletePodsOnSuccessTimeout }}
  {{- end }}
  template:
    metadata:
      labels:
        {{- include "common.labels.standard" . | nindent 8 }}
        {{- if .Values.additionalLabels }}
        {{- include "common.tplvalues.render" ( dict "value" .Values.additionalLabels "context" . ) | nindent 8 }}
        {{- end }}
      {{- if .Values.additionalAnnotations }}
      annotations: {{- include "common.tplvalues.render" ( dict "value" .Values.additionalAnnotations "context" . ) | nindent 8 }}
      {{- end }}
    spec:
      {{- if or .Values.global.imagePullSecrets .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- range .Values.global.imagePullSecrets }}
        - name: "{{ . }}"
        {{- end }}
        {{- range .Values.imagePullSecrets }}
        - name: "{{ . }}"
        {{- end }}
      {{- end }}
      serviceAccountName: {{ include "common.names.fullname" . | quote }}
      containers:
        - name: {{ .Chart.Name | quote }}
          securityContext:
            {{- toYaml .Values.containerSecurityContext | nindent 12 }}
          image: "{{ coalesce .Values.image.registry .Values.global.imageRegistry }}/{{ .Values.image.repository }}{{ if .Values.image.digest }}@{{ .Values.image.digest }}{{ else }}:{{ .Values.image.tag }}{{ end }}"
          imagePullPolicy: {{ .Values.image.imagePullPolicy | quote }}
          {{- with .Values.resources }}
          resources:
            {{ toYaml . | nindent 12 | trim }}
          {{- end }}
          env:
            - name: "NAMESPACE"
              value: {{ .Release.Namespace | quote }}
            - name: "CORE_MIDDLEWARE_STATEFULSET"
              value: {{ .Values.coreMiddleware.statefulSet | quote }}
            - name: "CORE_MIDDLEWARE_POD"
              value: {{ .Values.coreMiddleware.pod | quote }}
          command:
            - "/bin/bash"
            - "-c"
            - >
              until kubectl exec --namespace="${NAMESPACE}" ${CORE_MIDDLEWARE_POD} -c core-mw
              -- bash -c 'echo core-mw running...';
              do
                echo "waiting for container core-mw to be alive..."
                sleep 1
              done

              kubectl exec --namespace="${NAMESPACE}" ${CORE_MIDDLEWARE_POD} -c core-mw
              -- bash -c
              "mkdir -p /opt/open-xchange/ox-filestore"

              kubectl exec --namespace="${NAMESPACE}" ${CORE_MIDDLEWARE_POD} -c core-mw
              -- bash -c
              '/opt/open-xchange/sbin/initconfigdb
              --configdb-user "${MYSQL_USER}"
              --configdb-pass "${MYSQL_PASSWORD}"
              --configdb-host "${MYSQL_HOST}"
              --mysql-root-passwd "${MYSQL_PASSWORD}"
              --configdb-dbname "${MYSQL_DATABASE}"'
              || echo "error initconfigdb"

              kubectl exec --namespace="${NAMESPACE}" ${CORE_MIDDLEWARE_POD} -c core-mw
              -- bash -c
              'until nc -z localhost 8009; do echo "waiting for port 8009"; sleep 5; done'

              kubectl exec --namespace="${NAMESPACE}" ${CORE_MIDDLEWARE_POD} -c core-mw
              -- bash -c
              '/opt/open-xchange/sbin/registerfilestore
              -A "${MASTER_ADMIN_USER}" -P "${MASTER_ADMIN_PW}"
              -t /opt/open-xchange/ox-filestore
              -s "100000"'
              || echo "error: registerfilestore"

              kubectl exec --namespace="${NAMESPACE}" ${CORE_MIDDLEWARE_POD} -c core-mw
              -- bash -c
              '/opt/open-xchange/sbin/registerserver
              -A "${MASTER_ADMIN_USER}" -P "${MASTER_ADMIN_PW}"
              -n oxserver'
              || echo "error: registerserver"

              kubectl exec --namespace="${NAMESPACE}" ${CORE_MIDDLEWARE_POD} -c core-mw
              -- bash -c
              '/opt/open-xchange/sbin/registerdatabase
              -A "${MASTER_ADMIN_USER}" -P "${MASTER_ADMIN_PW}"
              --name "PRIMARYDB"
              --hostname "${MYSQL_HOST}"
              --dbuser "${MYSQL_USER}"
              --dbpasswd "${MYSQL_PASSWORD}"
              --master true'
              || echo "error: registerdatabase"

              kubectl --namespace="${NAMESPACE}" rollout restart statefulset ${CORE_MIDDLEWARE_STATEFULSET}
      restartPolicy: "Never"
  backoffLimit: 4
...
