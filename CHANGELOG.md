## [2.1.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-open-xchange-bootstrap/compare/v2.1.1...v2.1.2) (2024-10-07)


### Bug Fixes

* **opendesk-open-xchange-bootstrap:** Remove enabled from containerSecurityContext ([ce53cfd](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-open-xchange-bootstrap/commit/ce53cfd234ff55c54fe0a44d8f16189ba1913654))

## [2.1.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-open-xchange-bootstrap/compare/v2.1.0...v2.1.1) (2024-10-07)


### Bug Fixes

* **opendesk-open-xchange-bootstrap:** Fix additionalAnnotations identation ([619db12](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-open-xchange-bootstrap/commit/619db12283c0982d4bc72fcc565234d8085bb88d))

# [2.1.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-open-xchange-bootstrap/compare/v2.0.0...v2.1.0) (2024-09-22)


### Features

* **opendesk-open-xchange-bootstrap:** Add additionalAnnotations, additionalLabels and containerSecurityContext ([b684abc](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-open-xchange-bootstrap/commit/b684abc57aff728c5ed1b78940b155906bb05f85))

# [2.0.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-open-xchange-bootstrap/compare/v1.3.5...v2.0.0) (2024-07-07)


### Bug Fixes

* **opendesk-open-xchange-bootstrap:** Use normal job without hook ([0569673](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-open-xchange-bootstrap/commit/056967398e661ae057b7c573195865bc2f1b12f5))


### BREAKING CHANGES

* **opendesk-open-xchange-bootstrap:** Remove helm hook annotation

## [1.3.5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-open-xchange-bootstrap/compare/v1.3.4...v1.3.5) (2023-12-21)


### Bug Fixes

* Add GPG key, update README.md ([e06747e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-open-xchange-bootstrap/commit/e06747e49cb56fa5d25890f114104eaa95214eae))

## [1.3.4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-open-xchange-bootstrap/compare/v1.3.3...v1.3.4) (2023-12-21)


### Bug Fixes

* **chartname:** Rename to opendesk-open-xchange-bootstrap ([5d8b7bb](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-open-xchange-bootstrap/commit/5d8b7bb32fc71cf7ee7754af7d3a9b00f174b0b0))

## [1.3.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-open-xchange-bootstrap/compare/v1.3.2...v1.3.3) (2023-12-20)


### Bug Fixes

* **ci:** Add .common:tags: [] ([a542e9b](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-open-xchange-bootstrap/commit/a542e9bf1bc5c166bae5976b8eecda3c4801cfc5))

## [1.3.2](https://gitlab.opencode.de/bmi/opendesk/components/charts/opendesk-open-xchange-bootstrap/compare/v1.3.1...v1.3.2) (2023-12-20)


### Bug Fixes

* **ci:** Move repo to Open CoDE ([16df41a](https://gitlab.opencode.de/bmi/opendesk/components/charts/opendesk-open-xchange-bootstrap/commit/16df41aa2da38a62609ca5a9b8eb3fa65383edc6))

## [1.3.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/compare/v1.3.0...v1.3.1) (2023-08-11)


### Bug Fixes

* **sovereign-workplace-open-xchange-bootstrap:** Remove quotes from numeric value ([50319a0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/commit/50319a069f1ae1c01ba8a839a621ddd8eced4461))

# [1.3.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/compare/v1.2.3...v1.3.0) (2023-08-10)


### Features

* **sovereign-workplace-open-xchange-bootstrap:** Add image digest support ([c9f57d5](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/commit/c9f57d58f281215940419a32fba04d60598c84e6))

## [1.2.3](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/compare/v1.2.2...v1.2.3) (2023-08-08)


### Bug Fixes

* **sovereign-workplace-open-xchange-bootstrap:** remove jobs after completion ([db3b472](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/commit/db3b472547058ff03e6e01f8af0ccf6ec96d0c8f))

## [1.2.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/compare/v1.2.1...v1.2.2) (2023-07-19)


### Bug Fixes

* more limited RBAC rules ([df2b313](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/commit/df2b313e792f2cc484a1d6951192aeaba46a0e56))

## [1.2.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/compare/v1.2.0...v1.2.1) (2023-07-19)


### Bug Fixes

* typo ([3470d8a](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/commit/3470d8a53a789b45b0fe738cf1943e28b0f2aa8d))

# [1.2.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/compare/v1.1.0...v1.2.0) (2023-07-19)


### Features

* restart mw after initialization ([2c84660](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/commit/2c846604f17bf147f1c1dcc23129a01424544303))

# [1.1.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/compare/v1.0.2...v1.1.0) (2023-07-12)


### Features

* make podname of core-mw configurable ([e587fd5](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/commit/e587fd5ea903e5e5a4a77ffea910144af85c5306))

## [1.0.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/compare/v1.0.1...v1.0.2) (2023-07-12)


### Bug Fixes

* yamllint ([116b6a9](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/commit/116b6a96a66797c74eb72adb1f303e88da022468))

## [1.0.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/compare/v1.0.0...v1.0.1) (2023-07-12)


### Bug Fixes

* add missing default values ([adf6666](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/commit/adf6666cacdaf6a1d9185afdd99403a5147462bf))

# 1.0.0 (2023-07-12)


### Features

* Initial commit ([ec08c4a](https://gitlab.souvap-univention.de/souvap/tooling/charts/sovereign-workplace-open-xchange-bootstrap/commit/ec08c4ab1b27940dc899a6f458d94d1be398c8a6))
